package com.mayank.events;

import com.google.gson.Gson;

public class Event {
    private final long timestamp;
    private final String message;

    public Event(long timestamp, String message) {
        if (null == message) {
            throw new IllegalArgumentException("message cannot be null");
        }
        this.timestamp = timestamp;
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String make() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Event event = (Event) o;
        return timestamp == event.timestamp && message.equals(event.message);
    }

    @Override
    public int hashCode() {
        int result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (message.hashCode());
        return result;
    }
}
