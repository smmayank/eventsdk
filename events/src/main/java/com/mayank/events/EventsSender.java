package com.mayank.events;

public class EventsSender extends Thread {
    public static final int SUCCESS = 200;
    private static final long WAIT_TIMEOUT = 1000;
    private final Object lock;

    public EventsSender() {
        lock = new Object();
    }

    @Override
    public void run() {
        while (true) {
            while (!NetworkUtils.isConnected(EventsManager.getsInstance().getContext())) {
                synchronized (lock) {
                    try {
                        lock.wait(WAIT_TIMEOUT);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            EventPair pair = EventPersist.getInstance().poll();
            if (null == pair) {
                throw new IllegalStateException("Pair cannot be null");
            }
            int status = NetworkUtils.postCall(BuildConfig.SERVER_URL, pair.getEvent().make());
            EventPersist.getInstance().setStatus(pair, status);
        }
    }
}
