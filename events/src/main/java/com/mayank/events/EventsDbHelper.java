package com.mayank.events;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

public class EventsDbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "Events";
    private static EventsDbHelper sInstance;

    private EventsDbHelper(Context context) {
        super(context, DB_NAME, null, BuildConfig.DB_VERSION);
    }

    public static void init(Context context) {
        if (null == sInstance) {
            synchronized (EventsDbHelper.class) {
                if (null == sInstance) {
                    sInstance = new EventsDbHelper(context);
                }
            }
        }
    }

    public static EventsDbHelper getInstance() {
        if (null == sInstance) {
            throw new IllegalStateException("must call init first");
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EventPair.getCreateStatement());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public long persistPair(EventPair eventPair) {
        // store in db
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        long insert = writableDatabase.insert(EventPair.TABLE_NAME, null, eventPair.getValues());
        writableDatabase.endTransaction();
        writableDatabase.close();
        return insert;
    }

    public List<EventPair> getPairs() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor query = readableDatabase.query(EventPair.TABLE_NAME, null, null, null, null, null,
                null);
        List<EventPair> pairs = new LinkedList<>();
        if (null != query) {
            if (query.moveToFirst()) {
                do {
                    pairs.add(EventPair.create(query));
                } while (query.moveToNext());
            }
            query.close();
        }
        readableDatabase.close();
        return pairs;
    }

    public int deletePair(EventPair pair) {
        int delete;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        delete = writableDatabase.delete(EventPair.TABLE_NAME, pair.getClause(), pair.getArgs());
        writableDatabase.endTransaction();
        writableDatabase.close();
        return delete;
    }

}
