package com.mayank.events;

import android.app.Application;
import android.content.Context;

import java.util.Calendar;

public class EventsManager {

    private static EventsManager sInstance;
    private final Context context;
    private EventsSender sender;

    public EventsManager(Context context) {
        if (!(context instanceof Application)) {
            throw new IllegalArgumentException("init must be called from application context");
        }
        this.context = context;
        // do not change the order
        EventsDbHelper.init(context);
        sender = new EventsSender();
        sender.start();
    }

    public static void init(Context context) {
        if (null == sInstance) {
            synchronized (EventsDbHelper.class) {
                if (null == sInstance) {
                    sInstance = new EventsManager(context);
                }
            }
        }
    }

    public static EventsManager getsInstance() {
        if (null == sInstance) {
            throw new IllegalStateException("must call init first");
        }
        return sInstance;
    }

    public Context getContext() {
        return context;
    }

    public void sendMessage(String message) {
        Event e = new Event(Calendar.getInstance().getTimeInMillis(), message);
        EventsDelegation.getInstance().delegateEvent(e);
    }
}
