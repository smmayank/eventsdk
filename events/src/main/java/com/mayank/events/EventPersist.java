package com.mayank.events;

import android.os.Looper;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

class EventPersist {

    private static EventPersist sInstance;
    private PriorityBlockingQueue<EventPair> queue;

    private EventPersist() {
        queue = new PriorityBlockingQueue<>();
        initQueue();
    }

    public static EventPersist getInstance() {
        if (null == sInstance) {
            synchronized (EventPersist.class) {
                if (null == sInstance) {
                    sInstance = new EventPersist();
                }
            }
        }
        return sInstance;
    }

    private void initQueue() {
        if (Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalStateException("init should not be called from Main thread");
        }
        List<EventPair> pairs = EventsDbHelper.getInstance().getPairs();
        queue.addAll(pairs);
    }

    public void persist(Event e) {
        Long time = Calendar.getInstance().getTimeInMillis();
        EventPair eventPair = new EventPair(time, e);
        EventsDbHelper.getInstance().persistPair(eventPair);
        queue.offer(eventPair);
    }

    public EventPair poll() {
        return queue.poll();
    }

    public void setStatus(EventPair e, int status) {
        // this can be done via update rather than just delete and insert
        EventsDbHelper.getInstance().deletePair(e);
        if (EventsSender.SUCCESS != status) {
            persist(e.getEvent());
        }
    }
}
