package com.mayank.events;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

class EventsDelegation extends Handler {

    private static final int DELEGATE = 0;
    private EventPersist persistor;

    private EventsDelegation(Looper looper) {
        super(looper);
    }

    public static EventsDelegation getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void delegateEvent(Event e) {
        obtainMessage(DELEGATE, e).sendToTarget();
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case DELEGATE: {
                if (msg.obj instanceof Event) {
                    Event e = (Event) msg.obj;
                    if (null == persistor) {
                        persistor = EventPersist.getInstance();
                    }
                    persistor.persist(e);
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    private static class InstanceHolder {
        private static final EventsDelegation INSTANCE;
        private static final HandlerThread sThread;

        static {
            sThread = new HandlerThread(EventsDelegation.class.getSimpleName());
            sThread.start();
            INSTANCE = new EventsDelegation(sThread.getLooper());
        }
    }
}
