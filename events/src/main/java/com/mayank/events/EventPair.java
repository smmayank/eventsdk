package com.mayank.events;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

class EventPair implements Comparable<EventPair> {

    public static final String TABLE_NAME = "event_pair";
    private static final String WHERE_CLAUSE = Columns.TIMESTAMP + " = ?";

    private long timestamp;
    private Event event;

    public EventPair(long timestamp, Event event) {
        if (null == event) {
            throw new IllegalArgumentException("event cannot be null");
        }
        this.timestamp = timestamp;
        this.event = event;
    }

    public static EventPair create(Cursor query) {
        long timestamp = query.getLong(query.getColumnIndex(Columns.TIMESTAMP));
        long eventTimestamp = query.getLong(query.getColumnIndex(Columns.EVENT_TIMESTAMP));
        String data = query.getString(query.getColumnIndex(Columns.EVENT_DATA));
        Event e = new Event(eventTimestamp, data);
        return new EventPair(timestamp, e);
    }

    public static String getCreateStatement() {
        return "create table if not exists " + TABLE_NAME + " ( " + Columns.TIMESTAMP +
                " long primary key, " + Columns.EVENT_DATA + " text not null, " +
                Columns.EVENT_TIMESTAMP + " long not null)";
    }

    public Event getEvent() {
        return event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        EventPair eventPair = (EventPair) o;

        return timestamp == eventPair.timestamp &&
                !(event != null ? !event.equals(eventPair.event) : eventPair.event != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (event != null ? event.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(@NonNull EventPair another) {
        return (int) (timestamp - another.timestamp);
    }

    public ContentValues getValues() {
        ContentValues values = new ContentValues();
        values.put(Columns.TIMESTAMP, timestamp);
        values.put(Columns.EVENT_DATA, event.getMessage());
        values.put(Columns.TIMESTAMP, event.getTimestamp());
        return values;
    }

    public String getClause() {
        return WHERE_CLAUSE;
    }

    public String[] getArgs() {
        return new String[]{String.valueOf(timestamp)};
    }

    private static class Columns {
        public static final String TIMESTAMP = "timestamp";
        public static final String EVENT_DATA = "event_data";
        public static final String EVENT_TIMESTAMP = "event_timestamp";
    }

}